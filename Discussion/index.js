// CRUD OPERATIONS

/*
	- CRUD operations are the heart of any backend application.
	- CRUD stands for create, read, update, and delete.
	- MongoDB deals with objects as its structure for documents, we can easily create them by providing objects into our methods.
*/


// CREATE: Inserting documents

// INSERT ONE
/*
	Syntax:
		db.collectionName.insertOne({})

Inserting/Assigning values in JS objects:
	object.objectMethod({object})
*/
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});


// INSERT MANY
/*
	Syntax:
		db.users.insertMany([
			{objectA},
			{objectB}
		])
*/
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "5878787",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "5552252",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
	]);


// READ: Finding documents

// FIND ALL
/*
	Syntax:
		db.collectionName.find();
*/
db.users.find();


// Finding users with single arguments
/*
	Syntax:
		db.collectionName.find({field: value});
*/
db.users.find({firstName: "Stephen"});


// UPDATE: Updating documents
// Repeat Jane to be updated
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});


// UPDATE ONE
/*
	Syntax:
		db.collectnionName.updateOne({criteria} , {$set: (field:value)});
*/
db.users.updateOne(
	{firstName: "Jane"},
	{
		$set: {
			firstName: "Jane",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "0000000",
				email: "janegates@gmail.com"
			}, 
			courses: ["AWS", "Google Cloud", "Azure"],
			department: "infrastructure",
			status: "active"
		}
	}

);
db.users.find({firstName: "Jane"});


// UPDATE MANY
db.users.updateMany({department: "none"}
{
	$set: {
		department: "HR"
	}
}
);
db.users.find().pretty();

// REPLACE ONE
/*
	- Can be used if replacing the whole document is necessary
	- Syntax:
		db.collectionName.replaceOne({criteria} , {field: value});
*/
db.users.replaceOne({lastName: "Gates"}, {firstName: "Bill", lastName: "Clinton"});
db.users.find({firstName: "Bill"});


// DELETE: Deleting documents
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "0000000",
		email: "test@gmail.com"
			}, 
	courses: [],
	department: "none",
});
db.users.find({firstName: "Test"});

// Deleting a single document
/*
	Syntax:
		db.collectionName.deleteOne({criteria});
*/

db.users.deleteOne({firstName: "Test"});
db.users.find({firstName: "Test"});

db.users.deleteOne({age: 65});
db.user.find({firstName: "Jane"});

// Delete Many
db.users.deleteMany({
	courses: []
});


// Delete all
// db.users.delete();
/*
	- Be careful when using the "deleteMany" method. If no search criteria is provided, it will delete all documents.
	- DO NOT USE: db.collectionName.deleteMany();
	- Syntax:
		db.collectionName.deleteMany({criteria});
*/


// ADVANCED QUERIES

// Query an embedded document
db.users.find({contact: {phone: "5875698", email: "janedoe@gmail.com"}});


// Find the document with the email "janedoe@gmail.com"
// Querying on nested fields
db.users.find({"contact.email": "janedoe@gmail.com"});


// Querying an Array with exact elements
db.users.find({courses: ["React", "Laravel", "SASS"]});

// Querying an element without regard to order
db.users.find({courses: {$all:["React", "Laravel", "SASS"]});


// Make an array to query
db.users.insert({
	namearr: [
		{
			nameA: "Juan"
		},
		{
			nameB: "Tamad"
		}
		]
	}
});